﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Media3D;

namespace LanguageQuiz
{
    public class AnimatedTranslation: UserControl
    {
        public event EventHandler Completed;

        private CompositeTransform3D _transform;
        private TextBlock _text;
        private Storyboard _sb;
        private TimeSpan? _pauseTime;
        public bool IsPaused => _pauseTime.HasValue;

        public AnimatedTranslation(string word)
        {
            _transform = new CompositeTransform3D()
            {
            };
            _text = new TextBlock
            {
                Text = word,
                FontSize = 80,
                Transform3D = _transform,
                TextWrapping = Windows.UI.Xaml.TextWrapping.WrapWholeWords
            };
            Content = _text;

            const double FadeInDuration = 1.0;
            var moveIn = new DoubleAnimation()
            {
                From = 0.0,
                To = 1.0,
                Duration = TimeSpan.FromSeconds(FadeInDuration) 
            };
            Storyboard.SetTarget(moveIn, _text);
            Storyboard.SetTargetProperty(moveIn, nameof(_text.Opacity));

            var zAnim = new DoubleAnimation()
            {
                BeginTime = TimeSpan.FromSeconds(FadeInDuration),
                //From = -2000 * upscale,
                To = 1000,
                Duration = TimeSpan.FromSeconds(GameConstants.TranslationDuration - FadeInDuration),
                EasingFunction = new QuinticEase() { EasingMode = EasingMode.EaseIn },
            };
            Storyboard.SetTarget(zAnim, _transform);
            Storyboard.SetTargetProperty(zAnim, nameof(_transform.TranslateZ));

            _sb = new Storyboard()
            {
            };
            _sb.Children.Add(moveIn);
            _sb.Children.Add(zAnim);
            _sb.Completed += OnAnimationCompleted;
        }

        private void OnAnimationCompleted(object sender, object e)
        {
            Completed?.Invoke(this, EventArgs.Empty);
        }

        public void StartAnimation()
        {
            _sb.Begin();

            if (_pauseTime.HasValue)
            {
                _sb.Seek(_pauseTime.Value);
                _pauseTime = null;
            }
        }

        public void PauseAnimation()
        {
            _sb.Pause();
            _pauseTime = _sb.GetCurrentTime();
            _sb.Stop();
        }

    }
}
