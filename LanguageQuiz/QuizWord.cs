﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageQuiz
{
    public class QuizWord: PropertyChangedBase
    {
        public string[] Solutions { get; set; }
        public string[] Translations { get; set; }

        private int _currentTranslationIndex = -1;
        public int CurrentTranslationIndex { get => _currentTranslationIndex; private set => _currentTranslationIndex = value; }
        public string CurrentTranslation => CurrentTranslationIndex > -1 ? Translations[CurrentTranslationIndex] : null;

        public QuizWord()
        {
        }

        public bool NextTranslation()
        {
            if (CurrentTranslationIndex < Translations.Length - 1)
            {
                CurrentTranslationIndex++;
                OnPropertyChanged(nameof(CurrentTranslation));
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            CurrentTranslationIndex = -1;
        }
    }
}
