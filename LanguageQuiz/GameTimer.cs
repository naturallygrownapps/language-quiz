﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace LanguageQuiz
{
    public class GameTimer: IDisposable
    {
        private static readonly Lazy<GameTimer> _instance = new Lazy<GameTimer>(() => new GameTimer());
        public static GameTimer Instance => _instance.Value;

        private readonly DispatcherTimer _timer;

        public event EventHandler Tick;

        private GameTimer()
        {
            _timer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(1 / 60f)
            };
            _timer.Tick += OnTimerTick;
            _timer.Start();
        }

        private void OnTimerTick(object sender, object e)
        {
            Tick?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            _timer.Stop();
        }
    }
}
