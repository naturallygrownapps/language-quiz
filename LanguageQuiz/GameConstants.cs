﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanguageQuiz
{
    public static class GameConstants
    {
        public const double TranslationDuration = 5.0;
        public const int MaxPointsPerWord = 5;
        public const int WinningScore = 5;
        public const int InputCountdown = 30;
        public const int SolutionDisplayDuration = 5;
    }
}
