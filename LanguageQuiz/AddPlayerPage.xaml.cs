﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace LanguageQuiz
{
    public sealed partial class AddPlayerPage : Page
    {
        private WaitForControllerKeyPressTimer waitForControllerKeyPressTimer;
        private bool waitingForKeyboardInput = false;

        public AddPlayerPage()
        {
            this.InitializeComponent();
            KeyDown += OnKeyDown;
            NavigationCacheMode = NavigationCacheMode.Disabled;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            waitForControllerKeyPressTimer = new WaitForControllerKeyPressTimer();
            waitForControllerKeyPressTimer.NextKeyPress += OnControllerKeyPressed;
            ControllerManager.Instance.DisableKeyboardControllers();
        }

        private void Unregister()
        {
            waitForControllerKeyPressTimer.Dispose();
            KeyDown -= OnKeyDown;
            ControllerManager.Instance.EnableKeyboardControllers();
        }
        
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            Unregister();
            base.OnNavigatingFrom(e);
        }

        private void OnControllerKeyPressed(ControllerManager.ControllerKeyPress controllerKeyPress)
        {
            if (!string.IsNullOrWhiteSpace(nameInput.Text))
            {
                Unregister();
                Frame.Navigate(typeof(MainPage), new Player(nameInput.Text, controllerKeyPress.ControllerId));
            }
        }

        private void OnKeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Escape)
            {
                Frame.Navigate(typeof(MainPage), null);
            }
            else if (waitingForKeyboardInput)
            {
                AddKeyboardPlayer(e.Key);
            }
        }

        private void AddKeyboardPlayer(Windows.System.VirtualKey key)
        {
            if (!string.IsNullOrWhiteSpace(nameInput.Text))
            {
                Unregister();
                Frame.Navigate(typeof(MainPage), new Player(nameInput.Text, ControllerManager.GetControllerIdForKey(key)));
            }
        }

        private void OnUseKeyboardInputClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(nameInput.Text))
            {
                tbWaitForKeyInput.Visibility = Visibility.Visible;
                waitingForKeyboardInput = true;
            }
        }
    }
}
