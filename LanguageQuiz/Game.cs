﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;

namespace LanguageQuiz
{
    public class Game: PropertyChangedBase
    {
        private readonly ObservableCollection<QuizWord> _words = new ObservableCollection<QuizWord>();
        public ObservableCollection<QuizWord> Words => _words;

        private int _currentWordIndex = -1;
        public int CurrentWordIndex { get => _currentWordIndex; private set => _currentWordIndex = value; }
        public QuizWord CurrentWord => CurrentWordIndex > -1 ? _words[CurrentWordIndex] : null;
        private readonly DateTime _timestamp = DateTime.Now;
        public DateTime Timestamp => _timestamp;

        public Game()
        {
#if DEBUG
            //Words.Add(new QuizWord()
            //{
            //    Solution = "test",
            //    Translations = new List<string>()
            //    {
            //        "aaa",
            //        "bbb",
            //        //"ccc",
            //        //"ddd"
            //    }
            //});
#endif
        }

        public bool NextWord()
        {
            if (CurrentWordIndex < _words.Count - 1)
            {
                CurrentWordIndex++;
                var cw = CurrentWord;
                if (cw != null)
                    cw.Reset();
                OnPropertyChanged(nameof(CurrentWord));
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Game Load(string contents)
        {
            Game game = new Game();
            var json = JsonObject.Parse(contents);
            foreach(var jWord in json.GetNamedArray("words").Select(j => j.GetObject()))
            {
                var solutions = jWord.GetNamedArray("solutions").Select(j => j.GetString());
                JsonArray hintsArray;
                try
                {
                    hintsArray = jWord.GetNamedArray("translations");
                }
                catch(Exception)
                {
                    hintsArray = jWord.GetNamedArray("hints");
                }
                var translations = hintsArray.Select(j => j.GetString());
                var quizWord = new QuizWord()
                {
                    Solutions = solutions.ToArray(),
                    Translations = translations.ToArray()
                };
                game.Words.Add(quizWord);
            }
            return game;
        }
    }
}
