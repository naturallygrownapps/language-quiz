﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace LanguageQuiz
{
    public sealed partial class GamePage : Page, INotifyPropertyChanged
    {
        private static Random _random = new Random();

        public event PropertyChangedEventHandler PropertyChanged;

        private Game _game;
        public Game Game { get => _game; set => _game = value; }

        private AnimatedTranslation _currentWord;
        private Player _solutionExpectedPlayer;
        private Player SolutionExpectedPlayer {
            get => _solutionExpectedPlayer;
            set
            {
                var oldPlayer = _solutionExpectedPlayer;
                _solutionExpectedPlayer = value;
                if (oldPlayer != null && oldPlayer != _solutionExpectedPlayer)
                {
                    MarkPlayer(oldPlayer, Colors.Transparent);
                }
                if (_solutionExpectedPlayer != null && _solutionExpectedPlayer != oldPlayer)
                {
                    MarkPlayer(_solutionExpectedPlayer, Colors.Green);
                }
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsExpectingSolution));
            }
        }
        public bool IsExpectingSolution => _solutionExpectedPlayer != null;
        private CancellationTokenSource _countdownCancellation;
        private bool _isPlayerInputEnabled = false;

        public GamePage()
        {
            this.InitializeComponent();
            playerList.ItemsSource = PlayerManager.Instance.Players;
            this.NavigationCacheMode = NavigationCacheMode.Disabled;

            Loaded += OnPageLoaded;
            Unloaded += OnPageUnloaded;
        }

        private void OnPageLoaded(object sender, RoutedEventArgs e)
        {
            Windows.UI.Core.CoreWindow.GetForCurrentThread().KeyDown += OnPageKeyDown;
        }

        private void OnPageUnloaded(object sender, RoutedEventArgs e)
        {
            Windows.UI.Core.CoreWindow.GetForCurrentThread().KeyDown -= OnPageKeyDown;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.Game = (Game)e.Parameter;
            if (this.Game == null)
                throw new NullReferenceException("Missing game.");

            await NextWord();
            EnablePlayerInput();
        }

        private void EnablePlayerInput()
        {
            if (_isPlayerInputEnabled)
                return;

            GameTimer.Instance.Tick += OnGameTimerTick;
            _isPlayerInputEnabled = true;
        }

        private void DisablePlayerInput()
        {
            if (!_isPlayerInputEnabled)
                return;

            GameTimer.Instance.Tick -= OnGameTimerTick;
            _isPlayerInputEnabled = false;
        }

        private async void OnTranslationCompleted(object sender, EventArgs e)
        {
            _currentWord.Completed -= OnTranslationCompleted;
            if (!await NextTranslation(_game.CurrentWord))
            {
                await NextWord();
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            DisablePlayerInput();
            base.OnNavigatingFrom(e);
        }

        private async Task NextWord()
        {
            DisablePlayerInput();
            await Task.Delay(1000);
            
            if (_game.NextWord())
            {
                if (!await NextTranslation(_game.CurrentWord))
                {
                    // this should not happen, we need QuizWords with translations
                }
                foreach (var p in PlayerManager.Instance.Players)
                    p.IsEnabled = true;
                EnablePlayerInput();
                tbRemainingWords.Text = $"{(_game.CurrentWordIndex + 1)} / {_game.Words.Count}";
            }
            else
            {
                EndGame();
            }
        }

        private async Task<bool> NextTranslation(QuizWord word)
        {
            DisablePlayerInput();
            if (word.NextTranslation())
            {
                var translation = word.CurrentTranslation;
                if (_currentWord != null)
                    _currentWord.Completed -= OnTranslationCompleted;
                _currentWord = new AnimatedTranslation(translation);
                _currentWord.StartAnimation();
                _currentWord.Completed += OnTranslationCompleted;
                translationContainer.Content = _currentWord;
                translationContainer.Visibility = Visibility.Visible;
                EnablePlayerInput();
                tbRemainingPoints.Text = GetScoreForCurrentWord().ToString();
                return true;
            }
            else
            {
                await ShowSolution(word.Solutions);
                return false;
            }
        }

        private void PlayerWantsToEnterSolution(Player player)
        {
            DisablePlayerInput();
            SolutionExpectedPlayer = player;
            tbxSolution.Focus(FocusState.Programmatic);
            _currentWord.PauseAnimation();
            _countdownCancellation = new CancellationTokenSource();
            RunCountdown(_countdownCancellation.Token);
        }

        private static bool MatchesSolution(string[] expecteds, string solution)
        {
            string Normalize(string s)
            {
                return s.ToLowerInvariant().Replace("ä", "ae").Replace("ö", "oe").Replace("ü", "ue").Replace("ß", "ss");
            }

            // expected should never be empty
            if (string.IsNullOrWhiteSpace(solution))
                return false;

            return expecteds.Any(e => Normalize(e) == Normalize(solution));
        }

        private async void CheckSolution()
        {
            var solution = tbxSolution.Text;
            tbxSolution.Text = string.Empty;
            var player = SolutionExpectedPlayer;
            SolutionExpectedPlayer = null;

            if (MatchesSolution(Game.CurrentWord.Solutions, solution))
            {
                var score = GetScoreForCurrentWord();

                // Correct solution
                player.Score = player.Score + score;
                var sbCorrect = (Storyboard)Resources["sbCorrect"];
                sbCorrect.Begin();
                await Task.WhenAll(
                    Task.Delay(sbCorrect.Duration.TimeSpan),
                    ShowSolution(Game.CurrentWord.Solutions)
                );
                //if (player.Score >= GameConstants.WinningScore)
                //{
                //    EndGame();
                //}
                //else
                //{
                    await NextWord();
                //}
            }
            else
            {
                // Incorrect solution
                player.Score = player.Score - 1;
                player.IsEnabled = false;
                var sbIncorrect = (Storyboard)Resources["sbIncorrect"];
                sbIncorrect.Begin();
                await Task.Delay(sbIncorrect.Duration.TimeSpan);
                if (PlayerManager.Instance.Players.All(p => !p.IsEnabled))
                {
                    // everyone guessed incorrectly
                    await ShowSolution(Game.CurrentWord.Solutions);
                    await NextWord();
                }
                else
                {
                    // continue
                    _currentWord.StartAnimation();
                    EnablePlayerInput();
                }
            }

            try
            {
                await PlayerManager.Instance.SaveScores(Game);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.Fail("Could not save scores.", ex.ToString());
            }
        }

        private int GetScoreForCurrentWord()
        {
            var w = Game.CurrentWord;
            return Math.Max(GameConstants.MaxPointsPerWord - w.CurrentTranslationIndex, 1);
        }

        private void EndGame()
        {
            DisablePlayerInput();
            Player winner = null;
            int maxScore = int.MinValue;
            foreach(var p in PlayerManager.Instance.Players)
            {
                if (p.Score > maxScore)
                {
                    maxScore = p.Score;
                    winner = p;
                }
            }

            if (PlayerManager.Instance.Players.Count(p => p.Score == maxScore) > 1)
                winner = null;

            Frame.Navigate(typeof(GameEndPage), new GameEndResult(Game, winner));
        }

        private void OnGameTimerTick(object sender, EventArgs e)
        {
            // Do nothing if not started yet or we are waiting for player input.
            if (_currentWord == null || SolutionExpectedPlayer != null)
                return;

            var firstPressedPlayer = ControllerManager.Instance.GetAllControllerKeyPresses()
                    .Select(kp => PlayerManager.Instance.Players.FirstOrDefault(p => p.ControllerId == kp.ControllerId))
                    .Where(p => p != null && p.IsEnabled)
                    .ToArray()
                    // Randomize array to give equal chances when everyone presses at the same time.
                    .OrderBy(_ => _random.Next())
                    .FirstOrDefault();

            if (firstPressedPlayer != null)
            {
                PlayerWantsToEnterSolution(firstPressedPlayer);
            }
        }

        private void OnSolutionEntered(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key != Windows.System.VirtualKey.Enter)
                return;
            
            if (SolutionExpectedPlayer != null)
            {
                StopCountdown();
                CheckSolution();
            }
        }

        private void MarkPlayer(Player player, Color color)
        {
            for (int i = 0; i < playerList.Items.Count; i++)
            {
                var container = playerList.ContainerFromIndex(i) as ListViewItem;
                var panel = container?.ContentTemplateRoot as Panel;
                if (panel?.DataContext == player)
                {
                    panel.Background = new SolidColorBrush(color);
                }
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        private void StopCountdown()
        {
            _countdownCancellation?.Cancel();
        }

        private async void RunCountdown(CancellationToken token)
        {
            int remainingSecs = GameConstants.InputCountdown;
            while(remainingSecs > -1)
            {
                tbCountdown.Text = "(" + remainingSecs + ")";
                await Task.Delay(1000);
                if (token.IsCancellationRequested)
                    break;
                remainingSecs--;
            }

            if (!token.IsCancellationRequested)
                CheckSolution();
        }

        private async Task ShowSolution(string[] solutions)
        {
            translationContainer.Visibility = Visibility.Collapsed;
            var sb = new Storyboard();
            var anim = new DoubleAnimation()
            {
                To = 1.0,
                Duration = TimeSpan.FromSeconds(1)
            };
            Storyboard.SetTarget(anim, tbSolution);
            Storyboard.SetTargetProperty(anim, nameof(tbSolution.Opacity));
            sb.Children.Add(anim);
            tbSolution.Text = string.Join(" / ", solutions);
            tbSolution.Opacity = 0;
            sb.Begin();

            await Task.Delay(TimeSpan.FromSeconds(GameConstants.SolutionDisplayDuration));
            tbSolution.Text = string.Empty;
        }

        private void OnPageKeyDown(Windows.UI.Core.CoreWindow sender, Windows.UI.Core.KeyEventArgs args)
        {
            if (args.VirtualKey == Windows.System.VirtualKey.Escape && SolutionExpectedPlayer == null)
            {
                if (PausePanel.Visibility == Visibility.Collapsed)
                {
                    DisablePlayerInput();
                    PausePanel.Visibility = Visibility.Visible;
                    MainPanel.Visibility = Visibility.Collapsed;
                    _currentWord.PauseAnimation();
                }
                else
                {
                    EnablePlayerInput();
                    PausePanel.Visibility = Visibility.Collapsed;
                    MainPanel.Visibility = Visibility.Visible;
                    _currentWord.StartAnimation();
                }
            }
        }
    }
}
