﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace LanguageQuiz
{
    public class Player: PropertyChangedBase
    {
        private static class ColorGenerator
        {
            private static Color[] _allColors;
            private static Random _random;
            private static Color[] _excludedColors =
            {
                Colors.Black, Colors.DarkGray, Colors.DarkSlateGray, Colors.DimGray
            };

            static ColorGenerator()
            {
                _random = new Random();

                var colorsType = typeof(Colors).GetTypeInfo();
                _allColors = colorsType.DeclaredProperties
                    .Where(p => p.GetMethod != null && p.GetMethod.IsStatic)
                    .Select(p => p.GetValue(null))
                    .Where(v => v is Color c && !_excludedColors.Contains(c))
                    .Cast<Color>()
                    .ToArray();
            }

            public static Color GetRandom()
            {
                if (_allColors.Length == 0)
                    return Colors.White;
                return _allColors[_random.Next(_allColors.Length)];
            }
        }

        private static readonly SolidColorBrush DisabledBrush = new SolidColorBrush(Colors.Gray);
        public string Name { get; }
        public string ControllerId { get; }
        private SolidColorBrush _color;

        public SolidColorBrush Color {
            get => IsEnabled ? _color : DisabledBrush;
            set
            {
                _color = value; OnPropertyChanged();
            }
        }
        private int _score;
        public SolidColorBrush OriginalColor => _color;
        public int Score { get => _score; set { _score = value; OnPropertyChanged(); } }

        private bool _isEnabled = true;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        public Player(string name, string controllerId, SolidColorBrush color = null)
        {
            Name = name;
            ControllerId = controllerId;
            Color = color;
        }

        public void ChangeColor()
        {
            Color = new SolidColorBrush(ColorGenerator.GetRandom());
        }

        public void Reset()
        {
            IsEnabled = true;
            Score = 0;
        }
    }
}
