﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace LanguageQuiz
{
    public class WaitForControllerKeyPressTimer: IDisposable
    {
        public event Action<ControllerManager.ControllerKeyPress> NextKeyPress;

        public WaitForControllerKeyPressTimer()
        {
            GameTimer.Instance.Tick += OnTimerTick;
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            var kp = ControllerManager.Instance.GetNextControllerKeyPress();
            if (kp != null)
            {
                NextKeyPress?.Invoke(kp);
            }
        }
        
        public void Dispose()
        {
            GameTimer.Instance.Tick -= OnTimerTick;
        }
    }
}
