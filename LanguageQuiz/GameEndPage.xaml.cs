﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace LanguageQuiz
{
    public class GameEndResult
    {
        public readonly Game Game;
        public readonly Player Winner;

        public GameEndResult(Game game, Player winner)
        {
            Game = game;
            Winner = winner;
        }
    }

    public class StartNewGameCommand
    {
    }
    
    public sealed partial class GameEndPage : Page
    {
        public GameEndPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var result = (GameEndResult)e.Parameter;

            playerList.ItemsSource = PlayerManager.Instance.Players;
            tbWinner.Text = result.Winner != null ? ($"{result.Winner.Name} hat gewonnen!") : "";
        }

        private void OnBackToStartClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage), new StartNewGameCommand());
        }
    }
}
