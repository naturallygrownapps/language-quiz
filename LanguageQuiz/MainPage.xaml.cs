﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Gaming.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace LanguageQuiz
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private Game _currentGame;
        public Game CurrentGame { get => _currentGame; private set { _currentGame = value; OnPropertyChanged(); } }
        
        private Player _rightClickSelectedPlayer;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            playerList.ItemsSource = PlayerManager.Instance.Players;

            CreateNewGame();
            Load();
        }

        private async void Load()
        {
            SetProgressSpinner(true);
            await PlayerManager.Instance.RestorePlayers();
            SetProgressSpinner(false);
        }

        public void CreateNewGame()
        {
            foreach (var p in PlayerManager.Instance.Players)
            {
                p.Reset();
            }
            try
            {
                CurrentGame = LoadDefaultGame();
            }
            catch(Exception ex)
            {
                CurrentGame = new Game();
                MessageDialog msg = new MessageDialog(ex.ToString(), "Error");
                var ignored = msg.ShowAsync();
            }
        }

        private Game LoadDefaultGame()
        {
            var assembly = this.GetType().GetTypeInfo().Assembly;
            using (var resource = assembly.GetManifestResourceStream("LanguageQuiz.laenderquiz.json"))
            using (var sr = new StreamReader(resource))
            {
                var contents = sr.ReadToEnd();
                return Game.Load(contents);
            }
        }

        private void AddPlayerClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AddPlayerPage));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is Player p)
            {
                PlayerManager.Instance.AddPlayer(p);
            }
            else if (e.Parameter is StartNewGameCommand)
            {
                CreateNewGame();
            }

            GameTimer.Instance.Tick += OnTimerTick;
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            foreach(var kp in ControllerManager.Instance.GetAllControllerKeyPresses())
            {
                var player = PlayerManager.Instance.Players.FirstOrDefault(p => p.ControllerId == kp.ControllerId);
                player?.ChangeColor();
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            GameTimer.Instance.Tick -= OnTimerTick;
        }

        private void OnDeletePlayer(object sender, RoutedEventArgs e)
        {
            if (_rightClickSelectedPlayer != null)
            {
                PlayerManager.Instance.RemovePlayer(_rightClickSelectedPlayer);
            }
        }

        private void PlayerList_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            if (e.OriginalSource is FrameworkElement fe)
            {
                _rightClickSelectedPlayer = fe.DataContext as Player;
            }
        }

        private void StartGameClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(GamePage), CurrentGame);
        }

        private void SetProgressSpinner(bool visible)
        {
            if (visible)
            {
                gdProgress.Visibility = Visibility.Visible;
            }
            else
            {
                gdProgress.Visibility = Visibility.Collapsed;
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
