﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Gaming.Input;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace LanguageQuiz
{
    public class ControllerManager: IDisposable
    {
        public const string KeyboardControllerId = "Keyboard";

        public class ControllerKeyPress
        {
            public readonly string ControllerId;

            public ControllerKeyPress(string controllerId)
            {
                ControllerId = controllerId;
            }
        }

        private interface IControllerState
        {
            string Id { get; }
            bool HasJustPressedAnyButton();
            void Update();
        }

        private class ControllerState: IControllerState
        {
            private bool[] _prevButtonStates;
            private bool[] _curButtonStates;
            private GameControllerSwitchPosition[] _switchPositions;
            private double[] _axisValues;

            private readonly RawGameController _controller;

            public string Id => _controller.NonRoamableId;

            public ControllerState(RawGameController c)
            {
                _controller = c;
                _prevButtonStates = Enumerable.Repeat(false, c.ButtonCount).ToArray();
                _curButtonStates = Enumerable.Repeat(false, c.ButtonCount).ToArray();
                _switchPositions = Enumerable.Repeat(GameControllerSwitchPosition.Center, c.SwitchCount).ToArray();
                _axisValues = Enumerable.Repeat(0.0, c.AxisCount).ToArray();
            }

            public void Update()
            {
                var t = _prevButtonStates;
                _prevButtonStates = _curButtonStates;
                _curButtonStates = t;
                _controller.GetCurrentReading(_curButtonStates, _switchPositions, _axisValues);
            }

            public bool HasJustPressedAnyButton()
            {
                for (int i = 0; i < _controller.ButtonCount; i++)
                {
                    if (!_prevButtonStates[i] && _curButtonStates[i])
                        return true;
                }

                return false;
            }
        }

        private class KeyboardControllerState : IControllerState
        {
            private readonly VirtualKey _key;
            private readonly CoreWindow _wnd;
            private readonly string _id;
            public string Id => _id;

            private CoreVirtualKeyStates _curState;
            private CoreVirtualKeyStates _prevState;

            public KeyboardControllerState(VirtualKey key)
            {
                _key = key;
                _id = ControllerManager.GetControllerIdForKey(key);
                _wnd = CoreWindow.GetForCurrentThread();
            }

            public bool HasJustPressedAnyButton()
            {
                return !_prevState.HasFlag(CoreVirtualKeyStates.Down) && _curState.HasFlag(CoreVirtualKeyStates.Down);
            }

            public void Update()
            {
                _prevState = _curState;
                _curState = _wnd.GetKeyState(_key);
            }
        }


        private static readonly Lazy<ControllerManager> _instance = new Lazy<ControllerManager>(() => new ControllerManager());
        public static ControllerManager Instance = _instance.Value;

        public event EventHandler ConnectedControllersChanged;

        private readonly List<IControllerState> _controllers;
        private bool _keyboardControllersEnabled = true;
        public bool KeyboardControllersEnabled
        {
            get => _keyboardControllersEnabled;
            private set => _keyboardControllersEnabled = value;
        }

        private ControllerManager()
        {
            _controllers = new List<IControllerState>();
            RawGameController.RawGameControllerAdded += OnControllerAdded;
            RawGameController.RawGameControllerRemoved += OnControllerRemoved;
            GameTimer.Instance.Tick += OnTimerTick;

            var w = Windows.UI.Core.CoreWindow.GetForCurrentThread();
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            this.Update();
        }

        private void OnControllerRemoved(object sender, RawGameController e)
        {
            lock (_controllers)
            {
                _controllers.RemoveAll(c => c.Id == e.NonRoamableId);
            }
            NotifyConnectedControllersChanged();
        }

        private void OnControllerAdded(object sender, RawGameController e)
        {
            lock (_controllers)
            {
                if (!_controllers.Any(c => c.Id == e.NonRoamableId))
                {
                    _controllers.Add(new ControllerState(e));
                }
            }
            NotifyConnectedControllersChanged();
        }

        public void DisableKeyboardControllers()
        {
            KeyboardControllersEnabled = false;
        }

        public void EnableKeyboardControllers()
        {
            KeyboardControllersEnabled = true;
        }

        private async void NotifyConnectedControllersChanged()
        {
            var dispatcher = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher;
            await dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                ConnectedControllersChanged?.Invoke(this, EventArgs.Empty);
            });
        }

        public ControllerKeyPress GetNextControllerKeyPress()
        {
            return GetAllControllerKeyPresses().FirstOrDefault();
        }

        public IEnumerable<ControllerKeyPress> GetAllControllerKeyPresses()
        {
            lock (_controllers)
            {
                foreach (var c in _controllers)
                {
                    var isEnabled = KeyboardControllersEnabled || !IsKeyboardController(c.Id);
                    if (isEnabled && c.HasJustPressedAnyButton())
                    {
                        yield return new ControllerKeyPress(c.Id);
                    }
                }
            }
        }

        public void Update()
        {
            lock(_controllers)
            {
                _controllers.ForEach(c => c.Update());
            }
        }

        private bool IsKeyboardController(string controllerId)
        {
            return controllerId != null && controllerId.StartsWith(KeyboardControllerId);
        }

        public void AddKeyboardController(VirtualKey key)
        {
            lock(_controllers)
            {
                _controllers.RemoveAll(c => c.Id == GetControllerIdForKey(key));
                _controllers.Add(new KeyboardControllerState(key));
            }
        }

        public bool IsConnectedOrTryConnect(string controllerId)
        {
            if (IsKeyboardController(controllerId))
            {
                AddKeyboardController(GetKeyFromKeyboardControllerId(controllerId));
                return true;
            }

            lock(_controllers)
            {
                return _controllers.Any(c => c.Id == controllerId);
            }
        }

        public void Dispose()
        {
            RawGameController.RawGameControllerAdded -= OnControllerAdded;
            RawGameController.RawGameControllerRemoved -= OnControllerRemoved;
            GameTimer.Instance.Tick -= OnTimerTick;
        }

        public static string GetControllerIdForKey(VirtualKey key)
        {
            return $"{KeyboardControllerId}_{key.ToString()}";
        }

        private static VirtualKey GetKeyFromKeyboardControllerId(string id)
        {
            var keyStr = id.Substring(KeyboardControllerId.Length + 1);
            return (VirtualKey)Enum.Parse(typeof(VirtualKey), keyStr, true);
        }
    }
}
