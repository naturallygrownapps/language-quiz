﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI;

namespace LanguageQuiz
{
    public class PlayerManager
    {
        private static readonly Lazy<PlayerManager> _playerManager = new Lazy<PlayerManager>(() => new PlayerManager());
        public static PlayerManager Instance => _playerManager.Value;

        private List<Player> _knownPlayers = new List<Player>();

        private readonly ObservableCollection<Player> _players = new ObservableCollection<Player>();
        public ObservableCollection<Player> Players => _players;

        private const string StorageFile = "players.json";

        private PlayerManager()
        {
#if DEBUG
            //AddPlayer(new Player("rufus", "{wgi/nrid/0KLYTEb-e?D-HEUIdP-EZQ@-5=?-Y1n-B1[_lb-}\0"));
#endif
            ControllerManager.Instance.ConnectedControllersChanged += OnConnectedControllersChanged;
        }

        private void OnConnectedControllersChanged(object sender, EventArgs e)
        {
            UpdateConnectedPlayers();
        }

        public async Task RestorePlayers()
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await localFolder.GetFileAsync(StorageFile);
                var contents = await FileIO.ReadTextAsync(file);
                _knownPlayers = Deserialize(contents);
            }
            catch (Exception)
            {
                try
                {
                    var f = await localFolder.TryGetItemAsync(StorageFile);
                    if (f != null)
                        await f.DeleteAsync(StorageDeleteOption.PermanentDelete);
                }
                catch { }
            }
            UpdateConnectedPlayers();
        }

        public async Task SavePlayers()
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            try
            {
                var file = await localFolder.CreateFileAsync(StorageFile, CreationCollisionOption.ReplaceExisting);
                var serialized = Serialize(_knownPlayers);
                await FileIO.WriteTextAsync(file, serialized, Windows.Storage.Streams.UnicodeEncoding.Utf8);
            }
            catch (Exception ex)
            {
            }
        }

        public void AddPlayer(Player player)
        {
            foreach (var p in _knownPlayers.Where(p => p.ControllerId == player.ControllerId).ToArray())
            {
                _knownPlayers.Remove(p);
            }
            _knownPlayers.Add(player);
            UpdateConnectedPlayers();
        }

        public void RemovePlayer(Player player)
        {
            _knownPlayers.Remove(player);
            UpdateConnectedPlayers();
        }

        private void UpdateConnectedPlayers()
        {
            Players.Clear();
            foreach(var player in _knownPlayers)
            {
                if (ControllerManager.Instance.IsConnectedOrTryConnect(player.ControllerId))
                {
                    Players.Add(player);
                }
            }
        }

        private string Serialize(IEnumerable<Player> players)
        {
            var playersJson = new JsonArray();
            foreach(var p in players)
            {
                var json = new JsonObject
                {
                    { "name", JsonValue.CreateStringValue(p.Name) },
                    { "controllerId", JsonValue.CreateStringValue(p.ControllerId) },
                    { "color", JsonValue.CreateStringValue(p.OriginalColor != null ? p.OriginalColor.Color.ToString() : "") }
                };
                playersJson.Add(json);
            }

            return playersJson.ToString();
        }

        private List<Player> Deserialize(string contents)
        {
            var playersJson = JsonArray.Parse(contents);
            List<Player> players = new List<Player>();
            foreach(var pJson in playersJson.Select(i => i.GetObject()))
            {
                var colorStr = pJson.GetNamedString("color");
                Windows.UI.Xaml.Media.SolidColorBrush brush = null;
                if (!string.IsNullOrWhiteSpace(colorStr))
                {
                    var color = ParseColor(colorStr);
                    brush = new Windows.UI.Xaml.Media.SolidColorBrush(color);
                }
                var player = new Player(pJson.GetNamedString("name"), pJson.GetNamedString("controllerId"), brush);
                players.Add(player);
            }
            return players;
        }

        private static Color ParseColor(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            return Windows.UI.Color.FromArgb(a, r, g, b);
        }

        public async Task SaveScores(Game game)
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            var ts = game.Timestamp;
            var filename = $"{ts.Year}_{ts.Month}_{ts.Day}T{ts.Hour}_{ts.Minute}_{ts.Second}.scores";
            var file = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
            StringBuilder contents = new StringBuilder();
            foreach (var p in PlayerManager.Instance.Players)
            {
                contents.AppendLine($"{p.Name}:{p.Score}");
            }
            await FileIO.WriteTextAsync(file, contents.ToString(), Windows.Storage.Streams.UnicodeEncoding.Utf8);
        }

    }
}
